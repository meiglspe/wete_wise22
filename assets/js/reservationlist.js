// let reservationlistapp = new Vue({
//    el: '#reservationlist',
parasails.registerPage("reservationlist", {
    data: {
        datefrom: "",
        dateto: "",
        reservations: []
    },
    methods: {
        find: function () {
            let origin = window.location.origin
            let url = new URL(origin + '/api/v1/reservation/find2');
            url.searchParams.append("fromdate", this.datefrom);
            url.searchParams.append("todate", this.dateto);
            fetch(url)
                .then(res => res.json())
                .then(data => {
                    this.reservations = data;
                    data.forEach((res) => {
                        console.log(res)
                    })
                })
        }
    }
})