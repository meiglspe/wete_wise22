## Änderungen in `config/production.js`

Im Abschnitt `datastore` die Produktionsdatenbank konfigurieren.

Im Abschnitt `sockets:` die URL zum heroku deployment eintragen, für die App *lido* wäre dies:
    
    onlyAllowOrigins: [
       'https://lido.onrender.com'
    ],
    

Im Abschnitt `http:` den Eintrag `trust proxy` auskommentieren:

    trustProxy: true,

Der Port auf render ist 10000:

    port: 10000,

Im Abschnitt `custom:` den Eintrag `baseUrl` die URL zum heroku deployment eintragen, für die App *lido* wäre dies:

    baseUrl: 'https://lido.onrender.com',


## Änderungen in `tasks/register/prod.js`

Folgende Zeile auskommentieren:

//    'uglify',


## Deploy

* Jetzt müssen Sie noch ein `commit` für die Änderungen ausführen
* Account auf [GitLab.com](https://gitlab.com) oder [GitHub](https://github.com) anlegen
* Ein Projekt auf einer der beiden Platformen anlegen und das repo als remote mit namen `render` anlegen
* Jedes deploy wird über `git push render main` initialisiert
* Auf [render.com](https://render.com) registrieren und einloggen
* GitLab projekt verknüpfen und folgende Einstellungen verwenden:
  - Build Command: `npm install`
  - Start Command: `sails lift --hookTimeout=120000 --prod`


