// api/models/Order.js
module.exports = {
    attributes: {
        time: {
            type: 'string',  
            columnType: 'TIME',  
            required: true,
        },
        address: {
            type: 'string',  
            columnType: 'VARCHAR(120)',  
            required: true,
        },
        price: { 
            type: 'number',  
            columnType: 'DECIMAL (6,2)',  
        },
        meals: {
            collection: 'meal',
            via: 'orders'
        }
    }
  };