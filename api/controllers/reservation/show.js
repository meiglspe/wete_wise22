module.exports = {


    friendlyName: 'Show',
  
  
    description: 'Show reservation after reservation.',
  
  
    inputs: {
        id: {
            description: 'The number of the reservation to look up.',
            type: 'number',
            required: true
          }
    },
  
  
    exits: {
      success : {
        responseType: 'view',
        viewTemplatePath: 'pages/reservation/confirm'
      }
    },
  
  
    fn: async function (inputs) {

        let reservation = await Reservation.findOne({ id: inputs.id }).populate('reservationItems');
        if (!reservation) { throw 'notFound'; }
        return {
            message : "",
            reservation    
        };
  
    }
  
  
  };
  