module.exports = {

    friendlyName: 'Create',
  
    description: 'Create reservation.',
  
    inputs: {
      date : {
          type: 'string',
          required: true
      },
      places : {
          type: 'ref',
          required: true
      }  
    },
  
    exits: {
  
    },
  
  
    fn: async function (inputs) {
        //let date = "2022-12-24"
        //var today = new Date().toISOString().slice(0, 10);
        let resValues = {
            date: inputs.date,
            customer: this.req.session.userId
        }
        let reservation = await Reservation.create(resValues).fetch();
        let resItemValues = [];
        inputs.places.forEach(element => {
            resItemValues.push({
                place: element,
                reservation: reservation.id
            })
        });
        await ReservationItem.createEach(resItemValues);
        return {id: reservation.id};
    }
  };
  