/**
 * MealController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const redactPasswords = require("sails-mysql/lib/private/redact-passwords");
const Sails = require("sails/lib/app/Sails");

module.exports = {

    menu: async function (req, res) {
        let categories = await Category.find().populate("meals");
        res.view('pages/order/menu', { categories });
    },
   
    step1: async function (req, res) {
        res.view('pages/order/step1');
    },

    step2: async function (req, res) {
        let orderValues = {};
        orderValues.address = req.body.ort; 
        orderValues.time = req.body.zeit;
        req.session.order = orderValues;
        res.view('pages/order/step2', {"order": req.session.order, "basket": req.session.basket});
    },

    commit: async function (req, res) {
        let orderValues = req.session.order 
        orderValues.customer = req.session.userId;
        let order = await Order.create(orderValues).fetch();
        let basket = req.session.basket 
        let ids = [];
        basket.forEach(orderItem => {
          ids.push(orderItem.id)
        });
        await Order.addToCollection(order.id, 'meals', ids)

        req.session.basket = [];
        req.session.order = null;

        res.redirect('/order/confirmation/'+order.id);
    },

    confirmation: async function (req, res) {
        let order = await Order.findOne({ id: req.params.id}).populate("meals"); 
        res.view('pages/order/show', { order });
    },

    find: async function (req, res) {
        sails.log.debug("List all orders....")
        let orders = await Order.find().populate("meals");
        res.view ('pages/order/index', { orders } );
      },
};

