# 09-tutorial-vue

Demonstration of Vue in sailsjs

#### Vue Applications

In the [Reservation Applikation](/assets/js/reservation.js) a user can book a place.
It is included in the view [New Reservation Page](/views/pages/reservation/new.ejs). 

In the [Reservation List Applikation](/assets/js/reservationlist.js) a user can book a place.
It is included in the view [Reservation List](/views/pages/reservation/index.ejs). 


#### Vue Components

A Place is displayed as a [Reservation Component](/assets/js/components/lido-place.js) and used in [New Reservation Page](/views/pages/reservation/new.ejs).

### Backend

The backend api is defined as actions in the folder [reservations](/api/controllers/reservation/).